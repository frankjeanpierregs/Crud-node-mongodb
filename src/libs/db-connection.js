const mongoose = require('mongoose')

let db;

module.exports = function connection() {
    if (!db) {
        db = mongoose.createConnection('mongodb://localhost/crud-example', {useMongoClient :  true, useUnifiedTopology: true, useNewUrlParser: true});
        
    }
    return db;
}